<h1 align="center">MV-Forms</h1> <br>
<!--<p align="center">
    <img alt="mv-forms logo" title="mv-forms logo" src="#" width="300">
</p>-->
<div align="center">
  <strong>MV-Forms</strong>
</div>
<div align="center">
  Valais Media Library static forms.
</div>

<div align="center">
  <h3>
    <a href="https://valais-media-library.gitlab.io/mv-forms/">Access</a>
    <span> | </span>
    <a href="#documentation">Documentation</a>
    <span> | </span>
    <a href="#contributing">
      Contributing
    </a>
  </h3>
</div>

<div align="center">
  <sub>Built with ❤︎ in Valais by <a href="https://gitlab.com/valais-media-library/mv-forms/-/graphs/master">contributors</a>
</div>


## Table of Contents

- [Introduction](#introduction)
- [Features](#features)
- [Usage](#usage)
- [Documentation](#documentation)
- [Contributing](#contributing)
- [History and changelog](#history-and-changelog)
- [Authors and acknowledgment](#authors-and-acknowledgment)

## Introduction

[![license](https://img.shields.io/badge/license-MIT-green.svg?style=flat-square)](https://gitlab.com/valais-media-library/mv-forms/blob/master/LICENSE)
[![pipeline status](https://gitlab.com/valais-media-library/mv-forms/badges/master/pipeline.svg)](https://gitlab.com/valais-media-library/mv-forms/commits/master)


👉 [View online](https://valais-media-library.gitlab.io/mv-forms/)

This repository hosts forms for the Media Valais Library. Each HTML page is a form. The repository also keeps the mail templates used for sending data after the form validation in the `/email-templates` directory.

## Features

- Display static forms (HTML/CSS/JS)

## Documentation

Forms are made using [Vue.js](https://vuejs.org/), [Form.io](https://formio.github.io/formio.js/) or with [SurveyJs](https://surveyjs.io/). Each form is a single `html` file containing all logic. To create a form, duplicate an existing one and modify it using the tools listed above.

### Installation

[Download the zip file](https://gitlab.com/valais-media-library/mv-forms/-/archive/master/mv-forms-master.zip) from the latest release and unzip it somewhere reachable by your webserver.

### GitLab CI

This project's static Page are built by [GitLab CI](https://about.gitlab.com/gitlab-ci/) every time you push or update the repository, following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml). It expects to put all your HTML files in the `public/` directory.

## Contributing

We’re really happy to accept contributions from the community, that’s the main reason why we open-sourced it! There are many ways to contribute, even if you’re not a technical person. Check the [roadmap](#roadmap) or [create an issue](https://gitlab.com/valais-media-library/mv-forms/issues) if you have a question.

1. [Fork](https://help.github.com/articles/fork-a-repo/) this [project](https://gitlab.com/valais-media-library/mv-forms/)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request

## History and changelog

You will find changelogs and releases in the [CHANGELOG.md](https://gitlab.com/valais-media-library/mv-makerspace/blob/master/CHANGELOG) file.

## Roadmap

Nothing

## Authors and acknowledgment

* **Michael Ravedoni** - *Initial work* - [michaelravedoni](https://gitlab.com/michaelravedoni)

See also the list of [contributors](https://github.com/michaelravedoni/prathletics/contributors) who participated in this project.

* Forms are made in [Vue.js](https://vuejs.org/), with [Form.io](https://www.form.io/) or with [SurveyJs](https://surveyjs.io/)

## License

[MIT License](https://gitlab.com/valais-media-library/mv-forms/blob/master/LICENSE)
